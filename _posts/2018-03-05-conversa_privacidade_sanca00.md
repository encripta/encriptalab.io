---
layout: post
title: "Encripta em Sanca: Privacidade em tempos de ''Curte, Comenta e Compartilha''"
date: 2018-03-02
---

Olá São Carlos, a criptonave do Encripta está aterrizando em terras 'sancanianas' e para marcar nossa chegada estamos organizando uma atividade para discutirmos a situação da privacidade na internet atualmente! (:

Nos encontraremos no campus da USP, em dois horários diferentes:

* **dia 13/03 (terça feira) às 15h30**, na praça em frente ao CAASO;
* **dia 14/03 (quarta feira) às 19h15**, no palquinho em frente ao CAASO.

Veja a localização no [mapa](https://www.openstreetmap.org/search?query=-22.00722%2C-47.89677#map=19/-22.00722/-47.89677) (OpenStreetMap).

A atividade será uma roda de conversa descontraída sobre o estado atual da vigilância em massa (tanto a estatal quanto a empresarial), no Brasil e no mundo, como isso nos afeta e, principalmente, como nos tornarmos menos suscetíveis a essa vigilância: soluções livres existem e estão ao nosso alcance!

Esperamos que o clima entre as pessoas participantes seja respeitoso, para que assim todas possam se expressar. Por isso, antes de ir, dê uma lida em nossos [manifesto](encripta.org/manifesto) e [código de conduta](encripta.org/codigo_de_conduta).

Um aperitivo da atividade, contextualizando a privacidade na USP: em 2016, a USP firmou um [acordo](https://www.usp.br/imprensa/?p=62162) com o Google para fornecer "gratuitamente" à comunidade acadêmica a ferramenta "G Suite for Education". Emails e armazenamento ilimitados nos servidores do Google são algumas das supostas vantagens oferecidas pela empresa. Mas se a USP não paga nada, por que o Google faz tudo isso de graça? Porque a conta é paga com os dados da comunidade acadêmia uspiana!

# Resistindo à distopia - CryptoRave

Entre os dias 4 e 5 de maio de 2018, em São Paulo - SP, durante 36 horas, a [CryptoRave](https://cryptorave.org) (CR) traz atividades sobre segurança, criptografia, hacking, anonimato, privacidade e liberdade na rede. Essa é a 5ª edição do evento!  Estamos montando uma caravana de São Carlos para irmos todo mundo junto! Mais informações [aqui](/caravanascr18/).

A CryptoRave inspira-se na ação global e descentralizada da [CryptoParty](https://cryptoparty.in), a qual tem como objetivo difundir os conceitos fundamentais e softwares básicos de criptografia. Criada em 2014, a cada edição mais de 2500 pessoas participaram!

Além de ser um evento aberto e gratuito, a CryptoRave é financiada totalmente por doações! Estamos no meio da campanha de financiamento coletivo e, se não atingirmos a meta, não tem CryptoRave!

[Faça sua doação e receba recompensas sensacionais!](https://catarse.me/cryptorave2018) (catarse.me/cryptorave2018)


![divulgacao_conversa](/images/conversa_privacidade_sanca00/cartaz.png)
