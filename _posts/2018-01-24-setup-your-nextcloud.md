---
layout: post
title: "Configure seu próprio serviço de armazenamento em nuvem com Nextcloud"
date: 2018-01-24
---

Você provavelmente já usou ou usa alguns dos serviços de armazenamento privado em nuvem mais famosos como o Google Drive, Dropbox, iCloud, Box, Mega, Microsoft OneDrive, Amazon Drive e [tantos outros](http://computerworld.com.br/19-servicos-gratuitos-de-armazenagem-em-nuvem) disponíveis na grande rede mundial de computadores, desde quando me tornei um cidadão da Internet eu uso serviços de armazenamento em nuvem porque dispensa pendrives, HDs externos, é acessível de qualquer lugar com Internet além de ser barato ou gratuito — se você não precisa de tanto espaço de armazenamento.

Mas, nem tudo que é de graça é realmente gratuito, com a expansão de programas de vigilância em todo o mundo que visam monitorar a vida on-line de usuários do mundo todo possibilitando a investigação de dados em tempo real, além de monitorar, avaliar e recuperar o conteúdo. Há anos atrás o mais conhecido programa de vigilância do governo americano foi revelado pelo ex-técnico da CIA Edward Snowden, o [PRISM](https://olhardigital.com.br/video/saiba-como-funciona-o-prism,-a-nova-ameaca-a-privacidade-na-internet/36005), e mostrou que espionagem em tempo real não é coisa de filme de Hollywood.

Desde a relevação do PRISM, o programa de vigilância do governo americano, eu tenho usado [ferramentas para tentar burlar, evitar e dificultar a vigiância](https://www.privacytools.io/), usando Dropbox por anos houve épocas que eu encriptava tudo usando Veracrypt e então enviava para a nuvem (sim, old school), mais tarde o [Cryptomator](https://cryptomator.org/) começou a fazer esse trabalho e hoje eu optei por manter meu próprio serviço de armazenamento em nuvem usando [Nextcloud](https://nextcloud.com) como plataforma.

O [Nextcloud](https://nextcloud.com) faz com que qualquer pessoa com um servidor possa hospedar e distribuir seu próprio armazenamento e efetivamente ter sua própria nuvem! Aqui tem algumas vantagens:

- Controle sua nuvem, o software e o sistema operacional do servidor
- Fácil acesso em qualquer lugar com Internet
- Open source, sim, é aberto!
- Criptografia de ponta a ponta
- Aplicativos (plugins) que tornam a aplicação poderosa

Você pode ver mais detalhes e outros produtos do [Nextcloud aqui](https://nextcloud.com/). Vamos começar com instruções de como você pode configurar seu próprio serviço de armazenamento em nuvem!

## Infraestrutura

Você pode instalar o Nextcloud em:

- Servidores VPS — na [Digital Ocean](https://www.digitalocean.com/)  e [Vultr](https://www.vultr.com/) você encontra servidores a 2.50/5.00 USD ou se desejar um serviço brasileiro, a [Locaweb](https://www.locaweb.com.br/cloud/vps-locaweb/) tem servidores a 17,90 R$.
- Servidores web com CPainel por exemplo.
- [Hospedagem terceirizada](https://nextcloud.com/providers/)
## Configuração

Instalar e configurar o Nextcloud depende da infraestrutura que você escolheu e está usando, aqui tem alguns links com instruções recomendadas:
**VPS**

- [Como instalar e configurar o Nextcloud no Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-nextcloud-on-ubuntu-16-04)
- [Instalando e configurando Nextcloud no CentOs 7](https://www.linuxnaweb.com/instalando-nextcloud-no-centos-7/)
- [Como instalar o Nextcloud 12 Server no Debian 9](https://hcnlinuxblog.wordpress.com/2017/08/18/como-instalar-o-nextcloud-12-server-no-debian-9/)
- [Monte sua própria nuvem com NextCloud + HTTPS + OnlyOffice Ou Collabora Online](https://blog.remontti.com.br/1557)

**Servidores web**

- [CPanel com Softaculous](https://www.softaculous.com/apps/files/Nextcloud)

**Hospedagem terceirizada**
Esse tipo de hospedagem o provedor de serviço faz a configuração e mantém todo o serviço e você apenas recebe as credenciais de acesso a administração, você não precisará fazer nenhuma instalação.

----------

Quer mais? Conheça o [GitHub](https://github.com/nextcloud) do Nextcloud e a página de [Suporte](https://help.nextcloud.com/) se desejar contribuir ou ajuda.
