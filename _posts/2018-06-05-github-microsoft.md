---
layout: post
title: Microsoft comprou o Github, e agora?
date: 2018-06-05
---

Ontem (4 de junho de 2018), foi anunciado que a [Microsoft está comprando](https://tecnoblog.net/245953/microsoft-compra-github/) o [Github](https://github.com), a plataforma de hospedagem de código-fonte com controle de versão usando o Git mais usada do mundo. Motivadas principalmente por questões sobre privacidade e sobre a futura qualidade do serviço, milhares de pessoas já migraram seus projetos para outras soluções livres.

O Github é um serviço proprietário e centralizado. Aqui vamos apresentar algumas soluções livres e que podem ser auto-hospedadas em seu próprio servidor :)


## Gitlab


Software livre, pode ser [auto-hospedado](https://about.gitlab.com/installation) ou usado em alguma instância existente. O próprio Gitlab possui uma instância, o [gitlab.com](https://gitlab.com) (com repositórios privados mesmo na versão gratuita!), que é livre (Community Edition). Outra opção é o [0xacab](https://0xacab.org), instância livre do Gitlab hospedada pelo [Coletivo Riseup](https://riseup.net), gratuita e também com repositórios privados.

Outra função muito útil do Gitlab é o [Gitlab Pages](https://about.gitlab.com/features/pages/), que permite que você hospede seu site de forma gratuita (nosso site [está hospedado no Gitlab](https://gitlab.com/encripta/encripta.gitlab.io)). Outra coisa legal é que existe uma [proposta de federação](https://gitlab.com/gitlab-org/gitlab-ee/issues/4517) entre as diversas instâncias do Gitlab.

Logo após se iniciarem os rumores da compra do Github pela Microsoft, começou a campanha ***#movetogitlab***, que já conta com milhares de adesões. A migração do Github para o Gitlab é bem rápida, leva menos de 5 minutos. [Veja aqui](https://about.gitlab.com/2018/06/03/movingtogitlab/) como migrar seus projetos.

[Site oficial do projeto](https://about.gitlab.com)

## Gogs

Também software livre, pode ser [auto-hospedado](https://gogs.io/docs/installation) ou usado em alguma instância já existente. A instância mais popular é a [NotABug.org](https://notabug.org/), que também é gratuita e livre.

[Site oficial do projeto](https://gogs.io/)

## Sugestões

Gosta de alguma solução livre ao Github que não está por aqui? Manda pra gente que adicionamos! Nosso email é o encripta [arroba] riseup [ponto] net.
