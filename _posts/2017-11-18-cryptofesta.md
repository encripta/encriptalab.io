---
layout: post
title: "CryptoFesta Taubaté"
date: 2017-11-18
---

<img src="/images/cryptofesta_logo3.jpg" alt="logo" style="width: 600px;"/>
<br>

Com muita alegria anunciamos que no dia 18 de novembro acontecerá a primeira edição da CryptoFesta Taubaté! Será um evento com debates, oficinas e palestras sobre segurança, software livre, anonimato, privacidade e liberdade na rede, além de um festival de instalação de GNU/Linux. Tudo isso com o som da galera do <a href="https://www.facebook.com/technonarua">Techno na Rua</a>!

Nosso evento será aberto a todas pessoas, especialmente àquelas sem conhecimento prévio e que nunca frequentaram algum evento do tipo! É a oportunidade perfeita para discutir porque a privacidade na internet importa, quem tem interesse nos seus dados e como ter uma vida digital mais consciente e segura.

A CryptoParty Taubaté é inspirada no movimento internacional e descentralizado da [CryptoParty](https://cryptoparty.in), que tem como objetivo difundir conhecimentos sobre proteção no mundo virtual. No Brasil, temos como grande exemplo e inspiração a [CryptoRave](https://cryptorave.org), que acontece anualmente em São Paulo e conta com a presença de milhares de pessoas.

Não tem ideia do que seja criptografia ou nunca parou pra pensar sobre privacidade e segurança na internet? A CryptoFesta Taubaté é a chance perfeita para começar a entender a importância desses conceitos!

O evento ocorrerá no dia 18 de novembro de 2017, das 14h às 21h, no Solar da Viscondessa, com entrada livre e gratuita! Você também pode acompanhar as atualizações a respeito do evento pelo [Facebook](https://www.facebook.com/cryptofesta).

![solar](/images/solar.jpg)
Solar da Viscondessa: Rua Quinze de Novembro, 996 - Centro, Taubaté.
Para ver o mapa até lá, [clique aqui](https://www.openstreetmap.org/node/5168614106#map=19/-23.02724/-45.55902).


*Não serão toleradas nenhuma forma de assédio ou desrespeito a participantes do evento. Mais informações podem ser encontradas na nossa [política anti-assédio](/politica_anti_assedio).*

### Programação ###

<a name="papainoel"></a>
## <center> 14h30 - 15h45 </center> ##
#### <center>Papai Noel de Pós-Verdade e o Presente Escondido</center> ####

Papai Noel sabe de tudo e recompensa comportamento aprovado pelas autoridades.  Adultos tomam como verdade outros mitos de controle social.  Entra a Teia Mundial (WWW) e as máquinas de propaganda por trás deles perdem seus monopólios locais.  Agora, na competição globalizada por corações (cérebros poderiam distinguir falso e verdadeiro), querem censurar "fake news", o termo da era pós-verdade tanto para propaganda concorrente quanto para verdade encoberta.  

Tal censura não vai parar na mídia (de controle) social: buscas (centralizadas) na Teia já são censuradas e de outras formas distorcidas, e até assistentes de software (privativo), rodando em nossos próprios computadores móveis, podem acabar auxiliando não a nós, mas aos censores que controlam seu software e, escondendo-nos o presente, nosso futuro.  Para tomar decisões inteligentes, precisamos da verdade, e para obtê-la, precisaremos não só de educação para pensamento crítico, mídia social descentralizada e uma rede neutra, mas também liberdade de software em nossos próprios dispositivos de comunicação e computação.

__*Alexandre Oliva*__: Evangelizador do Movimento Software Livre.  Palestrante GNU. Conselheiro da FSF (Free Software Foundation) América Latina.  Ativista do grupo LibrePlanet São Paulo.  Mantenedor do GNU Linux-libre e co-mantenedor do GCC (GNU Compiler Collection), GNU binutils e GNU libc. Desenvolvedor de ferramentas de desenvolvimento GNU na Red Hat Brasil. Engenheiro de Computação e Mestre em Ciências da Computação formado na Unicamp.
<br />

<a name="vigilanciaglobal"></a>
## <center> 16h - 16h45 </center> ##
#### <center>Vigilância global, as democracias sob a ditadura dos algoritmos</center> ####

Nessa palestra debateremos os impactos globais do expansão tecnológica e aprofundamento das redes sociais nas sociedades do mundo, os aspectos da geopolitica implicitas nas ações governamentais dos países centrais e periféricos e suas relações com os grandes conglomerados capitalistas das empresas de tecnlogia. O fim da privacidade, expansão da espionagem, a economia especulativa de grandes bancos de dados a serviço da desestabilização das democracias no mundo.

__*Caio Augusttus*__: Musico, marxista e militante comunista. Graduado em tecnologia da informação com especialização na área de segurança da informação.
<br />

<a name="dadosdados"> </a>
## <center> 17h - 17h45  </center> ##
#### <center>Dados dados </center> ####

Em um mundo cada vez mais "inteligente", todos aspectos de nossa vida vão sendo ligados à internet. Nossas relações interpessoais são mediadas pelos algoritmos do Facebook, nosso aprendizado e acesso a informação pelos do Google e até nossos caminhos decididos pelos códigos do Uber. A proposta desta atividade é que pensemos em que dados são gerados em atividades do nosso dia-a-dia, como eles podem ser usados e, principalmente, como podemos buscar mais privacidade.

__*José Victor*__: Ativista do Movimento do Software Livre e membro do coletivo Encripta. Acredita que a privacidade, o anonimato e a ajuda mútua são ferramentas essenciais para construírmos uma sociedade livre.
<br />

<a name="gnulinuxparanoobs"></a>
## <center>  18h - 18h45 </center> ##
#### <center>GNU/Linux para noobs  </center> ####
Aprenda o essencial para quem está dando os primeiros passos em GNU/Linux e perca o medo de livrar-se de sistemas operacionais que não respeitam sua liberdade. Nesta palestra, serão abordados o movimento software livre, a comunidade GNU, como escolher sua primeira distribuição e dicas/hacks.

__*Daniela Morais*__: Desenvolvedora de Software, graduanda em Ciência da Computação pela UNICAMP, entusiasta do movimento software livre e por escrever códigos limpos.
<br />

<a name="castelodecriptografia"></a>
## <center>  19h - 19h45 </center> ##
#### <center>As Chaves do Castelo da Criptografia</center> ####
Nesta atividade serão abordados os seguintes tópicos:
* criptografia (conceito);
* álgebra modular e fatoração;
* criptografia simétrica e assimétrica;
* algoritmos de hash;
* segredo, autoria e não-repúdio;
* certificado digital;
* gpg;
* aplicações: Tor, Deep Web e Criptomoedas.

__*Paulo Oliveira Kretcheu*__: Paulo Kretcheu é Administrador de redes a mais de 15 anos e também é colaborador do projeto Debian. Falante de Esperanto, possui as certificações LPI-C, UCP, Novell NCLA e DCTS. É pós-graduado em Auditoria e Segurança, Engenheiro Mecânico e Professor Universitário. Apresentador do Vídeo Blog [kretcheu.com.br](http://kretcheu.com.br/)
<br />

<a name="anonimatoprivacidade"></a>
## <center> 20h - 20h45  </center> ##
#### <center> Anonimato, cebolas e privacidade na internet  </center> ####
O Tor é um software e uma rede operada por uma comunidade global. Nesta palestra apresentaremos o software, explicaremos como você pode plantar cebolas e a se engajar na Grande Causa!

__*Gustavo Gus*__: Gustavo Gus é fundador e organizador da [CryptoRave](https://cryptorave.org). 	Criada em 2014, a CryptoRave é um evento anual de hacking, segurança e privacidade.

<a name="http://encripta.org/cryptofesta/#installfest"></a>
# Festival de Instalação de GNU/Linux e Softwares Livres #

Teremos um espaço destinado à instalação do sistema operacional livre <a href="https://www.debian.org/index.pt.html">Debian</a> nos computadores das pessoas interessadas. Além disso, aproveite o espaço para conhecer e instalar programas livres em sua máquina ou celular, assim como discutir e tirar dúvidas sobre privacidade e criptografia!

 É importante que você faça o backup dos seus dados antes do evento se quiser fazer a formatação da sua máquina para instalar o Debian.

 <img src="/images/Apoiadores-02.png" alt="apoiadores" style="width: 1000px;"/>
 <br>
