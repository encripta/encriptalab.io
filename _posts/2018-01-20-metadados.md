---
layout: post
title: "Vejo com quem andas e digo quem tu és"
date: 2018-01-20
---

Um espião persegue uma pessoa de interesse. Anota os lugares em que ela vai, com quem se encontra, quanto tempo conversa com cada pessoa e a frequência de determinados compromissos. Esse espião não consegue ouvir as conversas, já que para isso teria que chegar perto demais, colocando em risco o segredo da missão. Mesmo assim, determina os círculos sociais, gostos e opiniões políticas da pessoa perseguida, conseguindo o suficiente para classificá-la como inimigo do Estado.

Métodos de vigilância como o anterior dependem diretamente da quantidade de mão de obra disponível, tornando a vigilância em massa impraticável. Nos tempos de hoje, porém, cada pessoa carrega consigo (voluntariamente) seu próprio espião pessoal. Nossos celulares rastreiam em tempo real para onde vamos, com quem nos encontramos e quanto tempo ficamos em cada lugar. Só no Brasil existem 241 milhões desses mini-espiões em circulação. Com a enorme quantidade de informação disponível e a crescente capacidade de processamento de dados, a vigilância rende resultados para deixar Sherlock Holmes com inveja.

Interceptar e analisar ligações e mensagens de uma população inteira pode ser muito custoso. Um método mais barato e também muito eficaz é analisar outro tipo de informação: os ***metadados***. É mais ou menos a ideia de um espião do século XVIII um pouco modernizado: para onde você vai, com quem se encontra, o que pesquisa, com que conversa e com que frequência. Para simplificar, pense em metadados como um ***registro de atividades***.


> Vocês estão com dificuldade em entender o termo "metadado"? Substitua por "registro de atividades". É isso que eles são.
>
>  -- <cite> Edward Snowden (@Snowden) [2 de novembro de 2015](https://twitter.com/Snowden/status/661305566967562240) </cite>

Tá, mas se nem tão lendo minhas conversas, que mal isso pode ter?

O documentário [Nothing to Hide](https://vimeo.com/nothingtohide) fez um experimento instalando um programa espião no celular de um voluntário. Esse programa não dava acesso a mensagens ou chamadas, apenas a metadados: onde o voluntário estava, seu histórico de navegação, com quem se comunicava e com que frequência. Em 30 dias de monitoramento, a quantidade de informações descobertas foi assustadora. Sua profissão, os círculos sociais dos quais faz parte, hobbies, orientação sexual, política e religiosa foram descobertas. Pode parecer pouco, mas num regime de exceção, por exemplo, essas são informações valiosíssimas.

![nothing_to_hide](/images/metadados/nothing_to_hide.png)

Ah... mas quem não deve não teme! Se você não faz nada errado, por que tem tanto medo?

[Este vídeo](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters) (20 minutos, legendas em português) do Glenn Greenwald vai responder muito bem sua pergunta. Deixo também dois textos sobre sociedade disciplinar e sociedade de controle: [Pokémon Go e a Sociedade de Controle](https://horizontesafins.wordpress.com/2016/08/06/pokemon-go-e-a-sociedade-de-controle/) e [Foucault e Deleuze: do poder disciplinar à sociedade de controle](http://www.ibamendes.com/2011/02/foucault-e-deleuze-do-poder-disciplinar.html).


Certo, celulares podem ser espiões, mas quem garante que isso acontece? Tá parecendo mais coisa de gente paranoica.

Quem diria, a teoria da conspiração era verdade! Em 2013, Edward Snowden revelou o [PRISM](https://pt.wikipedia.org/wiki/PRISM_(programa_de_vigil%C3%A2ncia)), um dos programas do sistema de ***vigilância global***  da NSA, Agência de Segurança dos EUA. Eram coletadas ligações (tanto as convencionais como as pela internet), mensagens, histórico de navegação, e-mails, arquivos armazenados e dados de redes sociais de pessoas de todo o mundo, incluindo no [Brasil](https://web.archive.org/web/20170814061727/http://g1.globo.com/fantastico/noticia/2013/07/brasil-e-um-grande-alvo-diz-jornalista-que-divulgou-denuncias-de-espionagem-americana.html). Grandes empresas, como Microsoft, Facebook, Google, Youtube, Skype e Apple eram parceiras da NSA nesse programa, como mostra a imagem abaixo, retirada de um dos slides vazados por Snoden.

![Empresas Prism](/images/metadados/empresas-prism.jpg "Empresas participantes do PRISM")

Outro fator que influencia a coleta de dados é a centralização da rede. Grande parte do fluxo de comunicação mundial passa pelos EUA ou é direcionada para lá, facilitando muito o trabalho da NSA. Após a revelação do PRISM, Dilma Roussef anunciou a construção de um novo [cabo submarino](http://www.brasil.gov.br/infraestrutura/2015/06/novo-cabo-submarino-ira-conectar-brasil-e-europa) que ligará o Brasil a Europa, sem passar pelos EUA.

![Fluxo de dados EUA](/images/metadados/cabos-submarinos.jpg "Fluxo de rede nos EUA")

Beleza, mas o que isso vai mudar na minha vida???

Na era do [Big Data](https://www.theguardian.com/technology/2014/jun/20/little-privacy-in-the-age-of-big-data), a análise de grandes quantidades de dados está em constante aperfeiçoamento. Informações de milhões de pessoas são estudadas e usadas para analisar tendências e categorizar as usuárias de acordo com suas características em comum, estratégia chamada de *profiling*. Algumas empresas se especializaram na coleta, análise e venda de dados, as ["corretoras de dados"](https://antivigilancia.org/pt/2015/05/data-brokers-e-profiling-vigilancia-como-modelo-de-negocios/) (*Data Brokers*, em inglês).

A mais conhecida de todos nós é a Serasa, que em 2007 se uniu à estadunidense Experian. Um dos serviços de destaque da Experian é o Mosaic, comercializado em 29 países. Sob o slogan “o poder da segmentação ao seu alcance”, o Mosaic Brasil “classifica a população brasileira em 11 grupos e 40 segmentos baseados em aspectos financeiros, geográficos, demográficos, de consumo, comportamento e estilo de vida.”

![Segmentos da Mosaic Brasil](/images/metadados/segmentos.jpg "Segmentos da Mosaic Brasil")

Um cliente do Mosaic pode obter, por exemplo, uma lista de pessoas em uma determinada região que se encaixam em diferentes perfis pré-determinados: “Seguindo a Vida na Periferia”, “Idosos da Agricultura Familiar do Norte e do Nordeste”, “Adultos Vulneráveis” ou “A Caminho da Aposentadoria nas Melhores Cidades”. Cada uma das pessoas dessa lista, nos bancos de dados da Serasa Experian, carrega consigo variadas informações de valor para o envio de marketing direcionado, como nome, endereço, e possivelmente seu [Score de Crédito](http://www.serasaconsumidor.com.br/score-credito/), um número calculado pela empresa que “indica, de maneira estatística, a probabilidade de inadimplência de determinado grupo ou perfil no qual um consumidor se insere, sem afirmar que ele esteve, está ou ficará inadimplente.”

Essas informações podem ser usadas para liberar ou negar empréstimos, regular preços de compras online, deixar seu plano de saúde mais caro (ou mesmo negar seu pedido de plano de saúde) e para enviar aqueles adoráveis [anúncios personalizados](https://antivigilancia.org/pt/2016/06/webcensus/). Para uma análise mais aprofundada sobre as corretoras de dados e o *profiling*, recomendo [este texto](https://antivigilancia.org/pt/2015/05/data-brokers-e-profiling-vigilancia-como-modelo-de-negocios/) da [Oficina Antivigilância](https://antigivilancia.org).

> Nós matamos pessoas baseados em metadados.
>
> -- <cite> Michael Hayden, ex-diretor da CIA</cite>

Para fins de vigilância governamental, a coleta indiscriminada de dados é usada como pretexto para combate ao terrorismo. Na realidade é como procurar uma agulha no palheiro, pois os governos passam a espionar toda sua população. [Este vídeo](https://www.youtube.com/watch?v=V9_PjdU3Mpo) (6 minutos, legendas em português) aborda o tema "Vigilância em Massa e Terrorismo".

Enfim, metadados não são brincadeira. Lembrando o que disse um ex-diretor da CIA, Michael Hayden, "nós matamos pessoas baseados em metadados".


**Atenção:** existem problemas relacionados a metadados em imagens, vídeos e outros tipos de arquivos que criamos. Este texto trata apenas de metadados de serviços de comunicação. Não é difícil encontrar na internet textos sobre como remover metadados de imagens e outros arquivos.

## Sistemas Centralizados, Descentralizados e Distribuídos

Atualmente, Facebook e Google [exercem inflência direta em mais que 70% do tráfego da internet mundial](https://staltz.com/the-web-began-dying-in-2014-heres-how.html). No Brasil, meia dúzia de empresas comandam toda a distribuição da internet pelo país. Google, Yahoo e Microsoft ([que espionam suas usuárias](http://articles.latimes.com/2013/oct/30/nation/la-na-nn-nsa-yahoo-google-data-20131030)) são responsáveis pelos e-mails de grande parte do mundo. Essa centralização faz com que, para coletar informações de milhões, bilhões de pessoas, seja preciso ter acesso apenas um restrito grupo de empresas.

Assim, um ***sistema centralizado*** é aquele em que há apenas uma central e todos que desejam utilizar esse serviço devem utilizá-la. Facebook, Skype, WhatsApp e Telegram são exemplos de serviços centralizados. Os registros de atividade de todas usuárias estão concentrados em apenas uma organização, tornando mais fácil a vigilância (tanto de um governo quanto da própria empresa, que pode vender nossos dados). Além disso, se a central sair do ar, todas as pessoas conectadas a ela perdem o acesso, sendo um alvo fácil para a censura (lembra do bloqueio do WhatsApp no Brasil?).

Apesar de centralizados, alguns serviços e softwares, como o [Signal](https://signal.org) e os serviços oferecidos pelo [Riseup](https://riseup.net), são conhecidos pelo respeito à privacidade de suas usuárias. O Signal, por exemplo, não guarda nenhum metadado das conversas de suas usuárias e o Riseup possui uma [política](https://riseup.net/en/privacy-policy#information-we-collect-and-retain) bem restrita de coleta de dados, prezando pela privacidade e apenas coletando o essencial para evitar abusos.

Uma dica é pensar no modelo de negócio da organização ou empresa que gerencia o serviço que você tem interesse. Signal e o Riseup são mantidos com doações. WhatsApp (comprado pelo Facebook por $22 bilhões), declaradamente compartilha informações sobre suas usuárias com empresas da "[Família Facebook](https://www.facebook.com/help/111814505650678)". Resumindo: os horários em que você está mais ativa, as pessoas com quem conversa e os locais dos quais acessa o WhatsApp são usados para vender propagandas personalizadas e, claro, ajudar agências de vigilância.

Não podemos esperar a solução vir de algum Estado ou empresa privada, já que eles são justamente os responsáveis por explorar a classe trabalhadora rotineiramente. Nós, usuárias, precisamos buscar soluções (preferencialmente descentralizadas ou distribuídas) para ***retomarmos o controle das nossas comunicações e dos nossos dados***. A imagem abaixo dá uma certa ideia do que cada tipo de serviço representa.

[![Sistemas Centralizados, Descentralizados e Distribuídos](/images/metadados/centralizacao.jpeg "Sistemas Centralizados, Descentralizados e Distribuídos")](https://medium.com/@bbc4468/centralized-vs-decentralized-vs-distributed-41d92d463868)


***Sistemas descentralizados*** (conhecidos também como ***federados***) são aqueles em que existem **diversas centrais que se comunicam entre si**. Qualquer pessoa ou grupo pode criar uma central que irá se comunicar com todas as outras. O exemplo mais notável de sistema descentralizado é o e-mail. Mesmo que poucas empresas concentrem a maior parte de usuárias, existem milhares de provedores de e-mail, todos conversando entre si, espalhados pelo mundo. A decentralização possui uma defesa melhor contra a coleta de metadados, já que as informações estão espalhadas entre as diferentes centrais. Também é mais eficiente contra a censura, já que derrubar uma central não é o suficiente para desconectar as pessoas que acessam o serviço por meio de outra central.

***Sistemas distribuídos***, também conhecidos como ***peer-to-peer*** (pessoa-para-pessoa ou p2p) são aqueles em que **cada usuária atua como um nó**. Qualquer nó pode se comunicar diretamente com outro nó, **sem a necessidade de uma central intermediária**. A coleta de dados (e metadados) é muito mais custosa, pois a informação está espalhada entre todas integrantes do sistema. Além disso, os sistemas distribuídos são praticamente imunes à censura, pois para tirá-lo do ar é preciso desconectar todas as pessoas que fazem parte da rede. O exemplo mais famoso de sistemas distribuídos são os torrents, usados para o compartilhamento de arquivos entre pessoas ao redor de todo o mundo.


### Softwares e serviços livres que respeitam sua privacidade

* Provedores de e-mails seguros, comprometidos com a privacidade de suas usuárias e que não vendem seus dados:
  - [Riseup](https://riseup.net): um coletivo que fornece emails, [listas de emails](https://riseup.net/en/lists), [VPN](https://riseup.net/en/vpn), [chat](https://riseup.net/en/chat), [pad](https://pad.riseup.net/) (para edição colaborativa de textos) e [armazenamento de arquivos](https://share.riseup.net/) para pessoas e grupos ativistas. Os dados dos usuários são guardados criptografados e dependem de doações para manter o serviço funcionando; servidores nos EUA.
  - [Tutanota](https://tutanota.com): serviço de emails criptografado, gratuito, e que utiliza software livre; servidores na Alemanha.
  - [Disroot](https://disroot.org): fornece emails, nuvem, e serviços online; são baseados nos princípios de liberdade, privacidade, federação e descentralização; servidores na Holanda.


* Comunicação:
  - Distribuídos:
    - [Ring](https://ring.cx) (vídeo, áudio e texto):  um programa de troca de mensagens e ligações livre e seguro, que suporta múltiplos dispositivos. Seu principal uso é para chamadas de áudio e vídeo (Linux/macOS/Windows/Android)
    - [Briar](https://briarproject.org) (texto): possui criptografia de ponta-a-ponta e funciona com internet, usando a rede Tor, e sem internet, usando Bluetooth (e futuramente WiFi). Resistente a censura e a falta de internet, como em casos de desastres naturais, ocupações ou manifestações. Ainda está em desenvolvimento, e um repositório com a versão inclompleta e instável pode ser encontrado [aqui](https://grobox.de/fdroid/) (Android)
    - [Tox](https://tox.chat) (vídeo, áudio e texto): um programa desenvolvido pela comunidade que permite a troca de mensagens e a realização de conferências de forma segura e sem intermediários. Está em intenso desenvolvimento, então não está muito estável (GNU/macOS/Windows/Android/iOS).
  - Descentralizados:
    - [Rede Matrix](https://matrix.org) (vídeo, áudio e texto): protocolo livre para comunicação em tempo real. Projetada para permitir que usuários com contas em diferentes provedores comuniquem-se por chat, chamadas de áudio e de vídeo. Possui diversos clientes, sendo que o mais conhecido é o [Riot](https://riot.im)(GNU/Windows/macOS/Android/iOS/Windows Phone)
    - [Linphone](https://linphone.org) (áudio): serviço de VoIP gratuito e cliente SIP. Software Livre, desenvolvido e mantido por uma empresa francesa (GNU/Windows/macOS/Android/iOS/Windows Phone)
  - Centralizados:
    - [Signal](https://signal.org) (áudio e texto): um aplicativo de mensagens e ligações criptografadas via internet, que utiliza uma criptografia reconhecida academicamente e comprovadamente segura [[download do .apk](https://signal.org/android/apk/)] (GNU/macOS/Windows/Android/iOS)
    - [Mumble](https://mumble.info) (áudio): feito para gamers e pessoas com internet internet. Você pode hospedar seu próprio servidor ou usar um dos publicamente disponíveis (existe um servidor público brasileiro). Possui a opção de gravar conferências com uma faixa para cada participante, útil para gravar entrevistas online (GNU/macOS/Windows/Android/iOS)


* Armazenamento e Sincronização de arquivos
  - [Syncthing](https://syncthing.net/): software livre, distribuído (peer-to-peer) e criptografado que permite sincronizar arquivos entre seus dispositivos, sem intermediários. Por exemplo, pode sincronizar a pasta de fotos do seu celular com seu computador.(GNU/macOS/Windows/Android)
  - [NextCloud](https://nextcloud.com): tome o controle e tenha sua própria nuvem. Software livre e também criptografado. Você pode usar o NextCloud em um servidor seu ou alugar um servidor para isso. [Aqui](/posts/setup-yout-nextcloud) explica melhor sobre como o NextCloud funciona e como usá-lo.


* Redes sociais livres:
  - Distribuídas:
    - [Twister](http://twister.net.co/): plataforma de microblogging peer-to-peer.
  - Descentralizadas:
    - dispora*:  uma rede social, semelhante ao facebook, mas federada. Isso significa que não existe um servidor central, mas vários servidores (“pods”) ao redor do mundo que se comunicam. Você pode criar uma conta no pod que preferir, ou até criar o seu próprio, e poderá normalmente se comunicar com pessoas de outros pods. No Brasil, os dois principais pods são [diasporabr.com.br](https://diasporabr.com.br/) e [diasporabrazil.org](https://diasporabrazil.org/). Esta rede social também é software livre, encriptada, e respeita a privacidade do usuário.
    - Mastodon: rede social livre, federada, semelhante ao twitter. Uma lista de instâncias disponíveis para você criar uma conta está disponível em [instances.social/list](https://instances.social/list)
    - GNU Social: uma rede social, também semelhante ao twitter, mas descentralizada. Criada pelo projeto [GNU](https://www.gnu.org/home.pt-br.html), você pode adicionar uma conta no servidor que quiser (inclusive criar seu próprio servidor) e se comunicar com usuários de outros servidores. Alguns servidores abertos são [Quitter.se](https://quitter.se/), [Quitter.no](https://quitter.no/), [LoadAverage](https://loadaverage.org/), e, no Brasil, existe o servidor do projeto LibrePlanet Brasil em [social.libreplanetbr.org](http://social.libreplanetbr.org/) (mais informações e criação de contas [aqui!](http://wiki.libreplanetbr.org/Servi%C3%A7os/))
    - [Pump.io](http://pump.io/): "Eu posto algo e meus seguidores veem. Essa é a ideia básica por trás do pump", retirado do site oficial do projeto. Você pode criar seu próprio servidor ou usar um dos públicos: [identi.ca](https://identi.ca) e [datamost.com](https://datamost.com/)
  - Centralizadas
    - [we.riseup.net](https://we.riseup.net/): rede muito usada por ativistas e coletivos. Software Livre e desenvolvida pelo [Riseup](https://riseup.net), tem tudo para você organizar seu grupo ou coletivo: listas de tarefas, páginas wiki (podem ser usadas para criar eventos públicos), listas de discussão e armazenamento de arquivos. Permite a criação de grupos, grupos de trabalho (chamados de comitês) e redes, que são grupos formados por vários grupos.
    - [Raddle](https://raddle.me/): fórum no estilo Reddit feito por ativistas. Foi criado após diversos grupos de esquerda serem banidos do Reddit, em 2016. Possui tolerância zero a fascismo e preconceito. Muitas discussões sobre temas anti-capitalistas.


#### Guias práticos
* [Guia de AutoDefesa Digital](https://autodefesa.fluxo.info/)
* [PRISM Break](https://prism-break.org/pt)
* [privacytools.io](https://privacytools.io)
* [A DIY Guide to Feminist Cybersecurit](https://hackblossom.org/cybersecurity/)
* [Segurança na caixa](https://securityinabox.org/pt/)
* [Autodefesa contra Vigilância](https://ssd.eff.org/pt-br)
