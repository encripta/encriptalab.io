---
layout: default
title: Projeto A.LO.CAR.
---

A.LO.CAR.
=========

As aventuras e desventuras vivenciadas por nós, seja como coletivo ou indivíduo, permitem que ao buscar estender informação e conhecimento a cerca da vigilância, privacidade, segurança e liberdade na internet àquelas pessoas a margem deste debate, possamos nos deparar com a realidade da documentação em português-brasileiro deste tema -- é escassa.
 
O projeto **A.LO.CAR.** emerge da necessidade em ampliar, tanto em quantidade quanto em qualidade, esta documentação se propondo a traduzir, produzir e disponibilizar livre e abertamente todos os documentos envolvidos no projeto. Com isso, pretende tornar o debate sobre vigilância, privacidade, segurança e liberdade na internet acessível a todas, em sua forma e linguagem.

#### **Aprender**
Para participar do projeto não é necessário ter conhecimento e/ou experiência prévia em  tradução de documentos, o que vemos como essencial é a **disposição em ensinar e em aprender**, não sendo este aprendizado restrito a parte técnica. Encorajamos a **participação de todas** aquelas pessoas que se interessarem pelo projeto, entendendo que as diferentes vivências das participantes  . Logo, esperamos que todas as participantes respeitem essa diversidade e que o convívio possa enriquecer o debate e o projeto, não sendo tolerado nenhuma forma de opressão e/ou ofensa.

O [Código de Conduta](/codigo_de_conduta/) do coletivo Encripta é válido  nos espaços do projeto A.LO.CAR e, caso você observe alguma conduta ofensiva, que não está coberta por no Código de Conduta, entre em contato conosco: encripta [arroba] riseup [ponto] net.

#### **Localizar**
Localizar é uma tarefa cujo objetivo é traduzir conteúdos de texto adaptando a tradução para a cultura do país ao qual se destina, normalmente se aplica a softwares e escritos de sites. 

#### **Compartilhar, Arquivar e Resistir**
###### **Tradução como forma de resistência!**

Acreditamos que o conhecimento quando não compartilhado perde sua função social libertadora. Por isso, ansiando que os produtos desse projeto alcancem pessoas para além da nossa *bolha*, iremos postá-los em nosso [site](encripta.org) e disponibilizá-los no [Internet Archive](link pra onde vai), encorajamos a todas que distribuam e compartilhem!


### Venha somar conosco!
Inscreva-se na lista de emails:

<FORM ACTION="https://lists.riseup.net/www" METHOD="POST">
  <b>Email:</b>
  <INPUT  NAME="email" SIZE="30">
  <INPUT TYPE="hidden" NAME="list" VALUE="projetoalocar">
  <INPUT TYPE="hidden" NAME="action" VALUE="subrequest">
  <INPUT TYPE="submit" NAME="action_subrequest" VALUE="Subscribe">
</FORM>
