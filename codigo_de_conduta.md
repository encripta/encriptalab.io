---
layout: default
title: Código de Conduta
---

O Coletivo Encripta existe com o intuito de **promover a inclusão** no debate sobre o meio digital e suas ferramentas, bem como a forma que este (pode) molda(r) o nosso cotidiano. Para nós, **expandir e incentivar a troca de conhecimento e práticas** sobre anti-vigilância e defesa digital se faz necessário na luta pela inclusão, sendo um caminho para aproximar e agregar pessoas com realidades e vivências diferentes. Neste sentido, é fundamental que todas as pessoas se sintam à vontade para compartilhar seu trabalho, opiniões e perspectivas. E, para isso, faz-se necessário que todos os **espaços**, digitais e/ou presenciais, ligados ao Coletivo Encripta sejam ambientes **que garantam e promovam o respeito, a inclusão, o diálogo, a escuta, a aprendizagem compartilhada e a colaboração**.

Para assegurar este caráter, o Coletivo Encripta mantém uma política antiassédio que rejeita todos os tipos de práticas e comportamentos homofóbicos, racistas, sexistas, transfóbicos ou de outra forma excludentes.

 - Use linguagem acolhedora e inclusiva-neutra. Comentários exclusivos ou piadas, ameaças ou linguagem violenta não são aceitáveis ​​em qualquer espaço, virtual e/ou presencial, ligado ao Coletivo Encripta.
 - Dê preferência para textos em português-brasileiro.
 - Não se dirija as outras pessoas de forma irritada, intimidante ou degradante. 
 - Atente às maneiras como as palavras que você escolhe podem afetar as outras pessoas. 
 - Não assedie as pessoas. O assédio inclui contato físico indesejado, atenção sexual ou contato social repetido. Saiba que o consentimento é explícito, consciente e contínuo - não está implícito. Se você não tem certeza se o seu comportamento em relação a outra pessoa é bem-vindo, pergunte-lhe. Se alguém lhe disser para parar, faça isso.
 - Respeite a privacidade e a segurança de terceiros. Não tire fotografias de outras pessoas sem a sua permissão. Tenha em mente que publicar (ou ameaçar publicar) a identidade pessoal de outras pessoas sem o seu consentimento é uma forma de assédio.
 - Atente à participação de outras pessoas, incluindo o uso do horário da conferência. Todas as pessoas deveriam ter a oportunidade de serem ouvidas. Em sessões em grupo, mantenha os comentários sucintos de modo a permitir o máximo envolvimento de todas as pessoas participantes. Não interrompa as outras pessoas se não estiver de acordo; Segure esses comentários até que as pessoas tenham terminado de falar.
 - Não seja uma pessoa espectadora. Se você ver algo inapropriado, fale.
 - Fascistas não são bem vindos.

## Quando o Código de Conduta se aplica?

Este Código de Conduta governa a participação em atividades e eventos promovidas pelo Coletivo Encripta e aplica-se a todas as pessoas participantes destes eventos e atividades.

A internet é vida real! Este Código de Conduta aplica-se em todos os espaços digitais ligados ao Coletivo Encripta (por exemplo, canais de bate-papo de grupo, redes sociais, etc).

As pessoas participantes que violarem este Código podem ser excluídas dos espaços ligados ao Coletivo.

## Como relato um problema relacionado ao Código de Conduta?

Por favor, fale conosco se você encontrar algum problema -- seja relacionado a uma situação específica ou a um aspecto mais geral do Coletivo Encripta. Você pode relatar o problema à equipe do Encripta pessoalmente (em situações ocorridas em eventos e atividades, por exemplo) ou por e-mail (encripta arroba riseup.net).

Você também pode reportar anonimamente problemas para a equipe do Coletivo Encripta -- mas use um endereço de e-mail onde você poderá receber respostas.

## Confidencialidade 

As informações compartilhadas com a equipe do Coletivo Encripta devem ser tratadas de forma confidencial e não serão mantidas após o fechamento do caso relatado. 
Em alguns casos, as informações precisarão ser compartilhadas para fins de implementação de uma resposta ao acolhimento deste Código de Conduta. Por exemplo, se uma pessoa que relata um problema está buscando uma desculpa de outra pessoa participante de espaços do Encripta, a equidade requer que a outra pessoa participante seja informada da questão que foi relatada e da identidade da pessoa que faz o relatório. No entanto, a informação nunca será compartilhada com outras pessoas sem o conhecimento e o consentimento da pessoa denunciante. 

A informação não pode ser mantida em sigilo quando a segurança de outras pessoas participantes de espaços do Coletivo Encripta estão em risco, ou quando existe a obrigação legal de denunciar (por exemplo, em determinadas situações envolvendo menores de idade).
